
function loadDetails(data) {
    for (const wyvern of data) {
        loadDetail(wyvern);
    }
}

function loadDetail(wyvern) {
    const identifier = wyvern.name;
    detailContainerName = loadDetailContainer(identifier);

    $(`#${detailContainerName}`).append(`
    <section class="wyvern-heading">
        <h1>${identifier}</h1>
        <img class="wyvern-image" src="./assets/img/${wyvern.icon}" alt="${wyvern.name} icon">
        <blockquote>
            ${wyvern.citation}
        </blockquote>
    </section>
    <section class="wyvern-detail">
        <h2>General information</h2>
        <div class="table-container">
            <table class="w-100 wyvern-characteristics">
                <tr>
                    <th>Species</th>
                    <th>Element(s)</th>
                    <th>Ailment(s)</th>
                    <th>Weaknesses</th>
                    <th>Weakness sign</th>
                    <th>Monster size</th>
                    <th>Generation</th>
                </tr>
                <tr>
                    <td>${wyvern.generalInformation.Species}</td>
                    <td id="${identifier}-detail-elements"></td>
                    <td id="${identifier}-detail-ailments"></td>
                    <td id="${identifier}-detail-weaknesses"></td>
                    <td>${wyvern.generalInformation.WeaknessSign}</td>
                    <td>Smallest : ${wyvern.generalInformation.MonsterSizes.Smallest} <br /> Tallest : ${wyvern.generalInformation.MonsterSizes.Tallest}</td>                    
                    <td>First</td>
                </tr>
            </table>
        </div>
    </section>
    <section class="wyvern-detail">
        <h2>Physiology</h2>
        <p>${wyvern.physiology}</p>
    </section>
    <hr class="separator" />
    <section class="wyvern-detail">
        <h2>Abilities</h2>
        <p>${wyvern.abilities}</p>
    </section>
    <hr class="separator" />
    <section class="wyvern-detail">
        <h2>Behavior</h2>
        <p>${wyvern.behavior}</p>
    </section>
    <hr class="separator" />
    <section class="wyvern-detail">
        <h2>Habitat</h2>
        <p>${wyvern.habitat}</p>
    </section>
    <nav id="${identifier}-wyvern-readmore">
        <a class="read-more" onclick="showPreviews()">Home</a>
    </nav>
    `);

    setWyvernIcons(identifier, wyvern.generalInformation);
    setReadMore(identifier, wyvern.readMore);
}

function setWyvernIcons(identifier, informations) {
    for (const element of informations.Elements) {
        $(`#${identifier}-detail-elements`).append(`<img class="icon" src="assets/img/${element.icon}" title="${element.title}" alt="${element.title}" /><br />`);
    }
    for (const ailment of informations.Ailments) {
        $(`#${identifier}-detail-ailments`).append(`<img class="icon" src="assets/img/${ailment.icon}" title="${ailment.title} (${ailment.type})" alt="${ailment.title} (${ailment.type})" /><br />`);
    }
    for (const weakness of informations.Weaknesses) {
        $(`#${identifier}-detail-weaknesses`).append(`<img class="icon" src="assets/img/${weakness.icon}" title="${weakness.title} (${weakness.type})" alt="${weakness.title} (${weakness.type})" />${weakness.appearedIn}<br />`);
    }
}

function setReadMore(identifier, readMore) {
    // Add a link for each wyvern with resemblances, found in the readMore array
    for (const wyvernName of readMore) {
        $(`#${identifier}-wyvern-readmore`).append(`<a class="read-more" onclick="showDetail(\'${wyvernName}\')">Read more : ${wyvernName}</a>`);
    }
}
