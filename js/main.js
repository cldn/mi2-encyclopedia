
const separator = '&';
const arithmeticOperators = {
    'GTEQ': '>=', // Operator : greater than or equal to
    'LTEQ': '<=', // Operator : lesser than or equal to
    'GT': '>', // Operator : greather than
    'LT': '<', // Operator : lesser than
    'EQ': '=' // Operator : equal to
};
const logicalOperators = {
    'NOT': 'NON', // Operator : not expression
    'AND': 'ET',
    'OR': 'OU'
}
const allowedKeywords = ['species', 'element', 'weakness', 'smallest', 'tallest']


function loadMenu(data) {
    for (const wyvern of data) {
        $(`#wyvern-menu`).append(`
        <li>
            <a onclick="showDetail('${wyvern.name}')">${wyvern.name}</a>
        </li>
        `);
    }
}

function loadPreviews(data) {
    for (const wyvern of data) {
        loadPreview(wyvern);
    }
}

function loadPreview(wyvern) {
    const identifier = wyvern.name;
    $(`#wyvern-preview-collection`).append(`
    <li id="${identifier}-preview">
        <section class="main-section">
            <h2>${wyvern.name}</h2>
            <figure>
                <figcaption>
                    ${wyvern.citation}
                </figcaption>
                <a href="assets/img/${wyvern.icon}" target="_blank">
                    <img src="./assets/img/${wyvern.icon}" alt="${wyvern.name} icon"/>
                </a>
            </figure>
            <div class="table-container">
                <table class="wyvern-characteristics">
                    <tr>
                        <th>Species</th>
                        <th>Element(s)</th>
                        <th>Weaknesses</th>
                        <th>Monster size</th>
                    </tr>
                    <tr>
                        <td class="preview-species">${wyvern.generalInformation.Species}</td>
                        <td id="${identifier}-elements" class="preview-elements"></td>
                        <td id="${identifier}-weaknesses" class="preview-weaknesses"></td>
                        <td class="preview-sizes">Smallest : ${wyvern.generalInformation.MonsterSizes.Smallest} <br /> Tallest : ${wyvern.generalInformation.MonsterSizes.Tallest}</td>
                    </tr>
                </table>
            </div>
            <div class="read-more-container">
                <a class="read-more" onclick="showDetail(\'${identifier}\')">Read more</a>
            </div>
        </section>
    </li>
    `);

    for (const element of wyvern.generalInformation.Elements) {
        $(`#${identifier}-elements`).append(`<img class="icon" src="assets/img/${element.icon}" title="${element.title}" alt="${element.title} damage" />`);
    }

    for (const weakness of wyvern.generalInformation.Weaknesses) {
        let apparitionsText = weakness.appearedIn;
        // If the current weakness isn't the last one, add a comma to separate them
        if ((wyvern.generalInformation.Weaknesses.indexOf(weakness) + 1) < wyvern.generalInformation.Weaknesses.length) {
            apparitionsText += ', ';
        }
        $(`#${identifier}-weaknesses`).append(`<img class="icon" src="assets/img/${weakness.icon}" title="${weakness.title}" alt="${weakness.title} ${weakness.type}"/> ${apparitionsText}`);
    }

}

function showPreviews() {
    hideAllDetails();
    $(`#wyvern-introduction`).css("display", "block");
    $(`#wyvern-preview-collection`).css("display", "block");
}

function loadDetailContainer(identifier) {
    detailContainerName = getDetailContainerName(identifier);
    $(`#wyvern-detail-collection`).append(
        `<li id="${detailContainerName}"></id>`
    );
    $(`#${detailContainerName}`).css("display", "none");
    return detailContainerName;
}

function hideAllDetails() {
    $(`#wyvern-detail-collection`).children().css("display", "none");
}

function showDetail(identifier) {
    hideAllDetails();
    $(`#wyvern-introduction`).css("display", "none");
    $(`#wyvern-preview-collection`).css("display", "none");
    $(`#${getDetailContainerName(identifier)}`).css("display", "block");
    scrollTop();
}

function getDetailContainerName(identifier) {
    return `${identifier}-detail`;
}

function scrollTop() {
    window.scrollTo(0, 0);
}

function prepSearchbar() {
    // Allow the user to press Enter in the searchbar
    $('#search-bar').on('keyup', function (event) {
        if (event.key === 'Enter') {
            showResults();
        }
    });
}

function showInformations() {
    alert(' \
        To search a word in a wyvern\'s characteristics, simply type a word and press "Search".\n \
        You can also do more specific searches about the following wyvern properties : \n \
        - species (text)\n \
        - element (text)\n \
        - weakness (text)\n \
        - smallest (numeric)\n \
        - tallest (numeric)\n \
        And using the following arithmetic operators : \n \
        - >=  (greater than or equal to..) \n \
        - <=  (lesser than or equal to..) \n \
        - >  (greather than..) \n \
        - <  (lesser than..) \n \
        - =  (equal to..) \n \
        Some examples of the queries you can do : \n \
        - species = Flying \n \
        - element = fire \n \
        - weakness = ice \n \
        - smallest >= 1150\n \
        - tallest < 2303.5 \n \
    ');
    const separator = '&';
    const arithmeticOperators = {
        'GTEQ': '>=', // Operator : greater than or equal to
        'LTEQ': '<=', // Operator : lesser than or equal to
        'GT': '>', // Operator : greather than
        'LT': '<', // Operator : lesser than
        'EQ': '=' // Operator : equal to
    };
    const logicalOperators = {
        'NOT': 'NON', // Operator : not expression
        'AND': 'ET',
        'OR': 'OU'
    }
    const allowedKeywords = ['species', 'element', 'weakness', 'smallest', 'tallest']
}

function showResults() {
    const search = $('#search-bar').val().trim();

    if (search.split(' ').length === 1) {
        // If the search is a single word, do a text search
        doTextSearch(search);
    } else {
        doParameterSearch(search);
    }
}

function doTextSearch(search) {
    let resultsCount = 0;
    // Iterate over the previews collection to check each preview's text
    $(`#wyvern-preview-collection`).children().each(function () {
        const wyvernMatches = $(this).text().includes(search);
        // Display the wyvern previews that contain the search text
        $(this).css('display', wyvernMatches ? 'block' : 'none');
        if (wyvernMatches) {
            resultsCount += 1;
        }
    });
    displaySearchResultsCount(resultsCount);
}

function doParameterSearch(search) {
    const noWhitespaceSearch = search.replace(new RegExp(' ', 'g'), '');

    for (const expr of noWhitespaceSearch.split(separator)) {

        // Find the arithmetic operator that the expression contains
        const operator = whichOperator(expr, arithmeticOperators);
        if (operator === undefined) {
            // If no arithmetic operator was found, alert the user
            alert(`no operator matching for expression '${expr}'`);
        } else {
            equation = expr.split(arithmeticOperators[operator]);
            if (equation.length !== 2) {
                // If the equation doesn't end up split in 2, there are too many operators in it: alert the user
                alert(`can not determine the right equation members for expression '${expr}': too many operators`);
                return;
            } else {
                const _property = equation[0]; // The left member of the equation is the name of the property to find in the wyvern characteristics
                const _value = equation[1]; // The right member of the equation is the value to compare

                // Do some consistency check depending on which property has been picked
                if (_property === allowedKeywords[0] || _property === allowedKeywords[1] || _property === allowedKeywords[2]) {
                    // If the property is about text, make sure the value is a text too
                    if (!(isNaN(_value))) {
                        alert(`property '${_property}' expects a text value`);
                        return;
                    }
                    // If the property is about text, make sure it receives a compatible operator (EQUAL being the only one)
                    if (arithmeticOperators[operator] !== arithmeticOperators.EQ) {
                        alert(`property '${_property}' allows text only and therefore only accepts EQUAL operator`);
                        return;
                    }
                } else if (_property === allowedKeywords[3] || _property === allowedKeywords[4]) {
                    // If the property is about a numerical characteristic, make sure the value is numerical too
                    if (isNaN(_value)) {
                        alert(`property '${_property}' expects a numerical value`);
                        return;
                    }
                }

                switch (_property) {
                    case allowedKeywords[0]:
                        searchBySpecies(_value);
                        break;
                    case allowedKeywords[1]:
                        searchByElement(_value);
                        break;
                    case allowedKeywords[2]:
                        searchByWeakness(_value);
                        break;
                    case allowedKeywords[3]:
                        searchBySmallest(operator, _value);
                        break;
                    case allowedKeywords[4]:
                        searchByTallest(operator, _value);
                        break;
                    default:
                        alert(`property '${_property}' is not allowed. See the search documentation for the allowed properties`);
                        break;
                }
            }
        }
    }
}

function whichOperator(search, operators) {
    for (operator in operators) {
        if (search.indexOf(operators[operator]) !== -1) {
            return operator;
        }
    }
}

function searchBySpecies(value) {
    $(`.preview-species`).each(function () {
        console.log($(this).text());
        const wyvernMatches = $(this).text().includes(value);
        // Display the wyvern previews that contain the value
        $(this).parent().parent().parent().parent().parent().parent().css('display', wyvernMatches ? 'block' : 'none');
    });
}

function searchByElement(value) {
    $(`.preview-elements`).each(function () {
        const wyvernMatches = $(this).html().includes(value);
        // Display the wyvern previews that contain the value
        $(this).parent().parent().parent().parent().parent().parent().css('display', wyvernMatches ? 'block' : 'none');
    });
}

function searchByWeakness(value) {
    $(`.preview-weaknesses`).each(function () {
        const wyvernMatches = $(this).html().includes(value);
        // Display the wyvern previews that contain the value
        $(this).parent().parent().parent().parent().parent().parent().css('display', wyvernMatches ? 'block' : 'none');
    });
}

function searchBySmallest(operator, value) {
    $(`.preview-sizes`).each(function () {
        // Parse the current smallest size from the preview-size table item
        const currentSmallest = Number($(this).text().match(/[\d.]+/g)[0]);
        // Evaluate the expression based on the current size, the operator, and the search parameter value
        let matches = eval(`${currentSmallest} ${arithmeticOperators[operator]} ${value}`);
        // Display the wyvern previews that contain the value
        $(this).parent().parent().parent().parent().parent().parent().css('display', matches ? 'block' : 'none');
    });
}

function searchByTallest(operator, value) {
    $(`.preview-sizes`).each(function () {
        // Parse the current tallest size from the preview-size table item
        const currentTallest = Number($(this).text().match(/[\d.]+/g)[1]);
        // Evaluate the expression based on the current size, the operator, and the search parameter value
        let matches = eval(`${currentTallest} ${arithmeticOperators[operator]} ${value}`);
        // Display the wyvern previews that contain the value
        $(this).parent().parent().parent().parent().parent().parent().css('display', matches ? 'block' : 'none');
    });
}


function displaySearchResultsCount(count) {
    $(`#search-results-count`).text(`${count} résultat(s) trouvé(s)`);
}
